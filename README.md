# I. Explain your design decisions

- For my design decisions I made sure to replace all branding and colors and decided to use my own personal freelance branding. I used some of my favorite desktop backgrounds as sample images.


# II. Explain your development decisions

- As for development, I tackled the responsive gallery first using breakpoints for desktop, tablet then mobile (100/50/33). I then turned to the hover state. I used pure CSS3 to have the required buttons fall from the top of the image. I then worked on header using IWI logo again and just adding placeholder search and nav. If it were a real site, I would use font-awesome icon replacements there to cut on load-time a bit. Next, I moved on to the footer, just adding placeholder text and switching up colors. I then tested the site in BrowserStack a staple at my old job. It let me test live environments of iPhone, iPad, Android, Windows and OS X (Safari, Chrome, Firefox, etc.) to make sure all audiences have the same experience on the page. I also made sure to add in sample SEO meta data and alt text/titles for images and added in a favicon.


# III. Explain areas for future improvement

- One future improvement/recommendation I have is to avoid hover states if not essential because they aren't supported on mobile. If essential I would code a mobile state that has the hover buttons permanently shown.


# IV. Add anything else you think is important

- I did the extra credit steps 1 and 3. Here is the URL for my freelance site where it is hosted: <http://ironworksinteractive.com/demo-project/> and I use Cloudflare to enhance overall performance of the page.
	